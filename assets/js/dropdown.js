let menubarHoverLock = true;

let initialize = function () {
    $(".top-bar_menu_entry").each(function (index) {
        $(this).html($(this).attr("name"));
    });
	$(".top-bar_menu_entry").each(function(index) {
		let elem = $(this);
		
        $("#top-bar_menu_container").append('<span class="top-bar_menu_dropdown" name=' + elem.attr("name") + "></span>");
		let dropdown = $(".top-bar_menu_dropdown[name='" + elem.attr("name") + "']");
        let pos;
        //elem.offset({ left: pos.left, top: 0 });
        pos = nucleoid.util.realElemCoords(elem);
        console.log(index, pos);
		dropdown.text(index + 1);
		elem.mouseover(function() {
			$(".top-bar_menu_dropdown").css("display", "none");
			dropdown.css({ left: pos.left, top: pos.top + pos.height });
			if (!menubarHoverLock) dropdown.css("display", "block");
		});
		$(".top-bar_menu_dropdown").on("click", function(e) {
			$(this).trigger("mouseover");
			e.stopPropagation();
		});
		$(".top-bar_menu_entry").on("click", function(e) {
			$(this).trigger("mouseover");
			e.stopPropagation();
		});
		$("body").click(function(e) {
			dropdown.css("display", "none");
			menubarHoverLock = true;
		});
	});
	$(".top-bar_menu_entry").on("mousedown", function() {
		if (!menubarHoverLock) {
			$(".top-bar_menu_dropdown").css("display", "none");
		}
		menubarHoverLock = !menubarHoverLock;
	});
};
let _module = {};
_module.initialize = initialize;
module.exports = _module;