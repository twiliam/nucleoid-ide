let _config = require("./config.js");
let _menu = _config.menu;

let _util = {
    realElemCoords: function (instance) {
        try {
            return instance[0].getBoundingClientRect();
        }
        catch {
            console.log("No element with specified selector.");
        }
    },
    realCoords: function (instance) {
        let a = [], i;
        for (i of instance) {
            a.push(i.getBoundingClientRect());
        }
        return a;
    }
}; 

let _sidebar = {
    width: 300,
    open: function () {
        $("#sidebar-menu").css("width", _sidebar.width);
        $("#workspace").css("transition-duration", "0.4s");
        $("#workspace").css("--left-clearance", _sidebar.width);
        $("#sidebar-menu").css("display", "inline-block");
    },
    close: function () {
        $("#workspace").css("transition-duration", "0ms");
        $("#sidebar-menu").css("width", "0px");
        $("#workspace").css("--left-clearance", "0px");
        $("#sidebar-menu").css("display", "none");
    }
};

let _command = {
    submit: function (command) {
        this.history.push(command);

        this.history_counter = this.history.length;

        if (this.command_bindings[command]) this.command_bindings[command]();

        if (command.startsWith(":js")) eval(command.substring(3, command.length));
    },
    command_bindings: {
        "reload": () => {
            window.location.reload(false);
        }
    },
    history: [],
    history_counter: 0
}
$(document).ready(() => {
    $(document).keydown((e) => {
        var evtobj = window.event ? event : e;
        if (evtobj.keyCode == 186 && evtobj.ctrlKey && evtobj.shiftKey)
            $("#command_bar_input").focus();
    });
    $(document).on("keydown", function (e) {
        if ($("#command_bar_input").is(":focus")) {
            var code = e.keyCode;
            if (code == 13) {
                //Enter
                _command.submit($("#command_bar_input").val());
                $("#command_bar_input").val("");
            }
            if (code == 38) {
                //Up Arrow
                if (_command.history_counter>0) _command.history_counter--;
                $("#command_bar_input").val(_command.history[_command.history_counter]);
            }
            if (code == 40) {
                //Down Arrow
                if (_command.history_counter <= _command.history.length - 1)
                    _command.history_counter++;

                var text = "";
                if (_command.history[_command.history_counter])
                    text = _command.history[_command.history_counter];

                $("#command_bar_input").val(text);
            }
        }
    });
    $("#sidebar-menu_open").click(_sidebar.open);
    $("#sidebar-menu_close").click(_sidebar.close);
});


//Workspace layout and tabs


//


//rendering menu entries


//new project dialog
let _np_dialog = {};
$("#new-project-dialog").dialog({
    autoOpen: false,
    resizable: false,
    width: 700,
    height: 500
});
_np_dialog.initial = $("#new-project-dialog");
_np_dialog.dialog = _np_dialog.initial.parent();

_np_dialog.show = function() {
    this.initial.dialog("open");
    this.dialog.show();
    this.initial.show();
    /*this.dialog.css("display", "block");
    this.initial.css("display", "block");*/
};
$(".ui-button").blur();

//Globals
nucleoid.util = _util;

//

let _dropdown = require("./assets/js/dropdown.js");
nucleoid.dropdown = _dropdown;
$(document).ready(function () {
    window.requestAnimationFrame(function () {
        _dropdown.initialize();
    });
});


