var menuEntry = function (name,action,...rest) {
    var template = {
        command_name: "No item name",
        action: function () {
            return true;
        },
        ui: null
    };
    template.command_name = name;
    template.action = action;
    template.ui = rest;
    return template;
};

module.exports.new_file = new menuEntry("New File", function () {
    //File API call for new file
    return 0;
}, "");
module.exports.new_project = new menuEntry("New Project", function () {
    //File API call for new project
    //Open project wizard
    return 0;
}, "");